<?php

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://coronavirus-19-api.herokuapp.com/countries');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $datas = json_decode(curl_exec($ch), true);
    curl_close($ch);

    $wwDeaths   = 0;
    $wwCases    = 0;

    foreach($datas AS $data){

        if($data['country'] == 'Indonesia'){

            $final = $data;

        }

        $wwDeaths   = $wwDeaths + $data['todayDeaths'];
        $wwCases    = $wwCases + $data['todayCases']; 

    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://coronavirus-19-api.herokuapp.com/all');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $wwData = json_decode(curl_exec($ch), true);
    curl_close($ch);

?>

<html>
    <head>

        <title>Indonesia Corona Virus Update</title>

        <meta name="description" content="Indonesia Corona Virus (COVID-19 Update">
        <meta name="keywords" content="corona, coronavirus, covid 19, covid, indonesia, data, update">
        <meta name="author" content="Aji Prakoso">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160640216-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-160640216-1');
        </script>

    </head>

    <body class="mb-5">
        <div class="container">
            <h2 class="mt-4 mb-3" align="center">Update kasus Virus Corona (COVID-19) di Indonesia</h2>
            <div class="row mt-3">
                <div class="col-md-3 col-6 mb-4">
                    <div class="card">
                        <div class="card-header" align="center">
                            <h5 class="card-title">Terkonfirmasi</h5>
                        </div>
                        <div class="card-body" align="center">
                            <h3><?=$final['cases'];?></h3>
                            <p>Hari ini + <?=$final['todayCases'];?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 mb-4">
                    <div class="card">
                        <div class="card-header" align="center">
                            <h5 class="card-title">Sembuh</h5>
                        </div>
                        <div class="card-body" align="center">
                            <h3><?=$final['recovered'];?></h3>
                            <p><b><?=number_format($final['recovered'] / $final['cases'] * 100,2);?>%</b> dari total kasus</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 mb-4">
                    <div class="card">
                        <div class="card-header" align="center">
                            <h5 class="card-title">Meninggal</h5>
                        </div>
                        <div class="card-body" align="center">
                            <h3><?=$final['deaths'];?> <br /></h3>
                            <p>Hari ini + <?=$final['todayDeaths'];?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6 mb-4">
                    <div class="card">
                        <div class="card-header" align="center">
                            <h5 class="card-title">Kasus Aktif</h5>
                        </div>
                        <div class="card-body" align="center">
                            <h3><?=$final['active'];?></h3>
                            <p><?=$final['critical'];?> Kritis</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4 mb-2">
                <div class="col-md-12 col-12">
                    <h3>Fakta Seputar COVID-19</h3>
                </div>
            </div>

            <div class="row mt-2 mb-2">
                <div class="col-md-12 col-12">
                    <table class="table table-striped table-hover">
                        <tr>
                            <td><b><?=count($datas);?></b> dari <b>195</b> atau <b><?=number_format(count($datas)/195*100,2);?>%</b> Negara di seluruh dunia dilaporkan terserang COVID-19.</td>
                        </tr>
                        <tr>
                            <td>Kasus yang dilaporkan di Indonesia adalah <b><?=number_format($final['cases']);?></b> kasus atau <b><?=number_format($final['cases']/$wwData['cases']*100,2);?>%</b> dari kasus COVID-19 di seluruh dunia (<b><?=number_format($wwData['cases']);?></b> Kasus)</td>
                        </tr>
                        <tr>
                            <td>Negara dengan pelaporan kasus COVID-19 terbanyak adalah <b><?=$datas[0]['country'];?></b> dengan jumlah kasus <b><?=number_format($datas[0]['cases']);?></b> dan <b> <?=number_format($datas[0]['deaths']);?> Orang</b> dinyatakan meninggal dunia.</td>
                        </tr>
                        <tr>
                            <td>Jumlah orang yang dilaporkan positif mengidap COVID-19 pada hari ini sebanyak <b><?=number_format($wwCases);?> Orang</b></td>
                        </tr>
                        <tr>
                            <td>Jumlah orang yang dilaporkan meninggal dunia diseluruh dunia karena COVID-19 hari ini sebanyak <b><?=number_format($wwDeaths);?> Orang</b></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row mt-2 mb-2">
                <div class="col-md-12" align="center">
                    Sumber : www.worldometers.info
                </div>
            </div>

        </div>

    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</html>